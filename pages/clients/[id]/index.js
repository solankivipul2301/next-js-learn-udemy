import {useRouter} from "next/router";

const ClientProjectPage = () => {
    const router = useRouter();
    const query = router.query;
    console.log({query})

    const loadProjectHandler = () => {
        router.push({
            pathname: '/clients/[id]/[clientProjectId]',
            query: {
                id: 'vip', clientProjectId: 'project-a'
            }
        });
    };

    return (
        <div>
            <h1>
                Client project page
            </h1>
            <button
                onClick={loadProjectHandler}
            >
                Load project A
            </button>
        </div>
    )
}

export default ClientProjectPage;