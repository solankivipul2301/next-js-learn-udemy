import {useRouter} from "next/router";
import Link from 'next/link';

const clientsPage = () => {
    const clients = [
        { id : 'max', name: 'Maxmilian' },
        { id : 'vip', name: 'Vipul' },
    ];

    return (
        <div>
            <h1>
                Client list page
            </h1>
            <ul>
                {clients.map(client => (
                    <li>
                        <Link
                            href={{
                                pathname: '/clients/[id]',
                                query: {id: client.id}
                            }}
                        >
                            {client.name}
                        </Link>
                    </li>
                ))}
            </ul>
        </div>
    )
}

export default clientsPage;